package ru.itis.la10;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.itis.la10.dto.Car;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/car/*")
public class CarByIdRESTServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {

        System.out.println("request.getContextPath():" + request.getContextPath());
        System.out.println("request.getPathInfo():" + request.getPathInfo());
        System.out.println("request.getServletPath():" + request.getServletPath());

        Long id = 0l;

        try {

            id = Long.parseLong(request.getPathInfo().substring(1));
        }catch (NumberFormatException e){
            try {
                response.sendError(500, "not found id");
            }catch(IOException ioException) {
                ioException.printStackTrace();
            }
        }

        Connection conn = DbWork.getConnection();

        try {
            PreparedStatement statement = conn.prepareStatement("SELECT id_car, model, id_driver FROM cars WHERE id_car = ?");
            statement.setLong(1,id);
            ResultSet rs = statement.executeQuery();

            Car car = null;

            while (rs.next()) {
                //получение данных из очередной строки результата;
                car = (new Car(rs.getInt("id_car"), rs.getString("model"), rs.getInt("id_driver")));

            }
            rs.close();
            statement.close();

            response.setHeader("Content-Type", "application/json");

            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), car);

        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }


        try {
            request.getRequestDispatcher("index.ftlh").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
