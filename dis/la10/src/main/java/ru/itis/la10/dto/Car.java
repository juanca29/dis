package ru.itis.la10.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car {
    private long id_car;
    private String model;
    private long id_driver;
}
