package ru.itis.la10;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.itis.la10.dto.Car;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/cars")
public class CarsRESTServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {

        Connection conn = DbWork.getConnection();

        try {
            Statement statement = conn.createStatement();

            ResultSet rs = statement.executeQuery(
                    "SELECT id_car, model, id_driver FROM cars");

            List<Car> lstcar = new ArrayList<>();

            while (rs.next()) {
                //получение данных из очередной строки результата;
                lstcar.add(new Car(rs.getInt("id_car"), rs.getString("model"), rs.getInt("id_driver")));

            }
            rs.close();
            statement.close();

            response.setHeader("Content-Type", "application/json");

            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), lstcar);

        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }


        try {
            request.getRequestDispatcher("index.ftlh").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
