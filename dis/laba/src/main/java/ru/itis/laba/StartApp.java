package ru.itis.laba;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class StartApp implements ServletContextListener {
    // marca el inicio y al final
    // listener reacciona a una accion en el servidor
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("aplication context start");
        DbWork.getInstance();
    }
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("aplication context end");
    }
}
