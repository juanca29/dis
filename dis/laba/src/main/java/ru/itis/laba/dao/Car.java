package ru.itis.laba.dao;

public class Car {
 public long id_car;
 public String model;
 public long id_driver;

    public Car(long id_car, String model, long id_driver) {
        this.id_car = id_car;
        this.model = model;
        this.id_driver = id_driver;
    }

    public long getId_car() {
        return id_car;
    }

    public void setId_car(long id_car) {
        this.id_car = id_car;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public long getId_driver() {
        return id_driver;
    }

    public void setId_driver(long id_driver) {
        this.id_driver = id_driver;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id_car=" + id_car +
                ", model='" + model + '\'' +
                ", id_driver=" + id_driver +
                '}';
    }
}
