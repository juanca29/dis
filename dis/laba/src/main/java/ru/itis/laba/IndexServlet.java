package ru.itis.laba;

import ru.itis.laba.dao.Car;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection conn = DbWork.getConnection();
        try {
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("SELECT id_car, model, id_driver FROM cars");

            List<Car> lstcar = new ArrayList<>();

            System.out.println("------------------------------------------------------------------");
            System.out.println("id_car | model | id_driver");
            System.out.println("------------------------------------------------------------------");

            while (rs.next()) {
                lstcar.add(new Car(rs.getInt("id_car"), rs.getString("model"), rs.getInt("id_driver") ));
                /*
                System.out.println(rs.getString("id_car") + "|"
                        + rs.getString("model") + "|" + rs.getString("id_driver"));

                 */
            }

            req.setAttribute("lstcar", lstcar);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            req.getRequestDispatcher("index.ftlh").forward(req,resp);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
