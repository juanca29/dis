package ru.itis;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Calculator extends JFrame implements ActionListener {
    //create formulary
    JPanel base = (JPanel) this.getContentPane();

    //create screen
    TextField screen = new TextField();

    // create buttons
    JButton bone = new JButton("1");
    JButton btwo = new JButton("2");
    JButton btree = new JButton("3");
    JButton bac = new JButton("AC");
    JButton bfour = new JButton("4");
    JButton bfive = new JButton("5");
    JButton bsix = new JButton("6");
    JButton bsum = new JButton("+");
    JButton bseven = new JButton("7");
    JButton beight = new JButton("8");
    JButton bnine = new JButton("9");
    JButton bminus = new JButton("-");
    JButton bmul = new JButton("*");
    JButton bcero = new JButton("0");
    JButton bdiv = new JButton("/");
    JButton bpoint = new JButton(".");
    JButton biquals = new JButton("=");


    //constructor
    public Calculator() {
        //formulary properties
        //null cause need manually put every button
        base.setLayout(null);
        //size of our window
        setSize(350, 470);
        //tittle
        setTitle("CALCULATOR");
        //visible formulary
        setVisible(true);
        //screen properties
        screen.setBounds(19, 8, 300, 70);
        screen.setFont(new Font("Tahoma", Font.BOLD, 20 ));
        add(screen);
        //screen buttons
        bone.setBounds(19, 90, 60, 60);
        add(bone);
        bone.addActionListener(this);

        btwo.setBounds(90, 90, 60, 60);
        add(btwo);
        btwo.addActionListener(this);

        btree.setBounds(160, 90, 60, 60);
        add(btree);
        btree.addActionListener(this);

        bac.setBounds(240, 90, 80, 60);
        add(bac);
        bac.addActionListener(this);

        bfour.setBounds(19, 155, 60, 60);
        add(bfour);
        bfour.addActionListener(this);

        bfive.setBounds(90,155, 60,60);
        add(bfive);
        bfive.addActionListener(this);

        bsix.setBounds(160,155, 60,60);
        add(bsix);
        bsix.addActionListener(this);

        bsum.setBounds(240,155, 80,60);
        add(bsum);
        bsum.addActionListener(this);

        bseven.setBounds(19,220, 60,60);
        add(bseven);
        bseven.addActionListener(this);

        beight.setBounds(90,220, 60,60);
        add(beight);
        beight.addActionListener(this);

        bnine.setBounds(160,220, 60,60);
        add(bnine);
        bnine.addActionListener(this);

        bminus.setBounds(240,220, 80,60);
        add(bminus);
        bminus.addActionListener(this);

        bmul.setBounds(19,285, 60,60);
        add(bmul);
        bmul.addActionListener(this);

        bcero.setBounds(90,285, 60,60);
        add(bcero);
        bcero.addActionListener(this);

        bdiv.setBounds(160,285,60,60);
        add(bdiv);
        bdiv.addActionListener(this);

        bpoint.setBounds(240,285,80,60);
        add(bpoint);
        bpoint.addActionListener(this);

        biquals.setBounds(19,350,300,60);
        add(biquals);
        biquals.addActionListener(this);

    }


    public static void main(String[] args) {
        new Calculator();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //get all clicks and print in screen
        if(e.getSource()== bone){
            if (screen.getText().equals("")){
                screen.setText("1");
            }else{
                screen.setText(screen.getText()+"1");
            }
        }
        if(e.getSource()== btwo){
            if (screen.getText()==""){
                screen.setText("2");
            }else{
                screen.setText(screen.getText()+"2");
            }
        }
        if(e.getSource()== btree){
            if (screen.getText()==""){
                screen.setText("3");
            }else{
                screen.setText(screen.getText()+"3");
            }
        }
        if(e.getSource()== bfour){
            if (screen.getText()==""){
                screen.setText("4");
            }else{
                screen.setText(screen.getText()+"4");
            }
        }
        if(e.getSource()== bfive){
            if (screen.getText()==""){
                screen.setText("5");
            }else{
                screen.setText(screen.getText()+"5");
            }
        }
        if(e.getSource()== bsix){
            if (screen.getText()==""){
                screen.setText("6");
            }else{
                screen.setText(screen.getText()+"6");
            }
        }
        if(e.getSource()== bsum){
            if (screen.getText()==""){
                screen.setText("+");
            }else{
                screen.setText(screen.getText()+"+");
            }
        }
        if(e.getSource()== bseven){
            if (screen.getText()==""){
                screen.setText("7");
            }else{
                screen.setText(screen.getText()+"7");
            }
        }
        if(e.getSource()== beight){
            if (screen.getText()==""){
                screen.setText("8");
            }else{
                screen.setText(screen.getText()+"8");
            }
        }
        if(e.getSource()== bnine){
            if (screen.getText()==""){
                screen.setText("9");
            }else{
                screen.setText(screen.getText()+"9");
            }
        }
        if(e.getSource()== bminus){
            if (screen.getText()==""){
                screen.setText("-");
            }else{
                screen.setText(screen.getText()+"-");
            }
        }
        if(e.getSource()== bmul){
            if (screen.getText()==""){
                screen.setText("*");
            }else{
                screen.setText(screen.getText()+"*");
            }
        }
        if(e.getSource()== bcero){
            if (screen.getText()==""){
                screen.setText("0");
            }else{
                screen.setText(screen.getText()+"0");
            }
        }
        if(e.getSource()== bdiv){
            if (screen.getText()==""){
                screen.setText("/");
            }else{
                screen.setText(screen.getText()+"/");
            }
        }
        if(e.getSource()== bpoint){
            if (screen.getText()==""){
                screen.setText(".");
            }else{
                screen.setText(screen.getText()+".");
            }
        }

        if (e.getSource()==bac){
            screen.setText("");
        }
        if (e.getSource()==biquals){
            String str = screen.getText();
            for (int i = 0; i <str.length(); i++) {
                char ca = str.charAt(i);
                if (ca == '+'){
                    String first = str.substring(0,i);
                    String second = str.substring(i+1, str.length());
                    Double value = Double.parseDouble(first)+ Double.parseDouble(second);
                    screen.setText(String.valueOf(value));
                }
                if (ca == '-'){
                    String first = str.substring(0,i);
                    String second = str.substring(i+1, str.length());
                    Double value = Double.parseDouble(first)- Double.parseDouble(second);
                    screen.setText(String.valueOf(value));
                }
                if (ca == '*'){
                    String first = str.substring(0,i);
                    String second = str.substring(i+1, str.length());
                    Double value = Double.parseDouble(first)* Double.parseDouble(second);
                    screen.setText(String.valueOf(value));
                }
                if (ca == '/'){
                    String first = str.substring(0,i);
                    String second = str.substring(i+1, str.length());
                    Double value = Double.parseDouble(first)/ Double.parseDouble(second);
                    screen.setText(String.valueOf(value));
                }
            }
        }



    }
}
