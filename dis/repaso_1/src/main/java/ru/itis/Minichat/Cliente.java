package ru.itis.Minichat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Cliente implements Runnable {

    private int puerto;
    private String mensaje;
    public Cliente(int puerto, String mensaje){
        this.puerto = puerto;
        this.mensaje = mensaje;

    }


    @Override
    public void run() {
        String host = "127.0.0.1";
        Socket sc;
        // string de entrada desde el cliente
        DataInputStream in;
        // string de respuesta para el servidor
        DataOutputStream out;
        try {
            sc = new Socket(host, puerto);

            out = new DataOutputStream(sc.getOutputStream());

            out.writeUTF(mensaje);



            sc.close();

        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }
}
