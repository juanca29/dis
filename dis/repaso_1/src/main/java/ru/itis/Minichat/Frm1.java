package ru.itis.Minichat;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;

public class Frm1 extends javax.swing.JFrame implements Observer {
    public Frm1() {
        this.getRootPane().setDefaultButton(this.btnEnviar);
        Servidor s = new Servidor(5000);
        s.addObserver(this);
        Thread t = new Thread(s);
        t.start();
    }
    private void btnEnviarActionPerformed(ActionEvent e) {
        String mensaje = "1: "+ this.textEnviar.getText() + "\n";
        this.textField.append(mensaje);
        Cliente c = new Cliente(6000, mensaje);
        Thread t = new Thread(c);
        t.start();
    }


    public static void main(String[] args) {
        new Runnable() {
            public void run() {
                new Frm1().setVisible(true);
            }
        };
    }
    private JButton btnEnviar;
    private JScrollPane scrollPane;
    private javax.swing.JTextArea textField;
    private javax.swing.JTextField textEnviar;

    @Override
    public void update(Observable o, Object arg) {

    }
}
