package ru.itis.Minichat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Observable;

public class Servidor extends Observable implements Runnable{
    private int puerto;

    public Servidor(int puerto){
        this.puerto = puerto;
    }



    @Override
    public void run() {
        // socket client
        Socket sc = null;
        // string de entrada desde el cliente
        DataInputStream in;
        // string de respuesta para el servidor
        DataOutputStream out;
        try {
            ServerSocket servidor = new ServerSocket(puerto);
            System.out.println("servidor iniciado");
            while (true) {
                sc = servidor.accept();
                System.out.println("el cliente conectado");
                in = new DataInputStream(sc.getInputStream());
                /// leo el mensaje que me envia
                String mensaje = in.readUTF();
                System.out.println(mensaje);
                this.setChanged();
                this.notifyObservers(mensaje);
                this.clearChanged();



                sc.close();
                System.out.println("el cliente se desconecto");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
