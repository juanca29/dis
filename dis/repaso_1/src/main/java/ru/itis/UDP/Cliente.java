package ru.itis.UDP;

import java.io.IOException;
import java.net.*;

public class Cliente {
    public static void main(String[] args) {
        int puertoServidor = 10000;
        byte[] buffer = new byte[1024];

        try {
            InetAddress direccionServidor = InetAddress.getByName("localhost");
        // de inicio no damos el puerto no en cesario y lo elije ramdom
            DatagramSocket socketUDP = new DatagramSocket();

            String mensaje = "hola mundo desde el cliente";
            buffer = mensaje.getBytes();
            DatagramPacket pregunta = new DatagramPacket(buffer, buffer.length, direccionServidor, puertoServidor);
            socketUDP.send(pregunta);

            //recibir

            DatagramPacket peticion = new DatagramPacket(buffer, buffer.length);

            socketUDP.receive(peticion);
            mensaje = new String (peticion.getData());
            System.out.println(mensaje);
            socketUDP.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
