package ru.itis.UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Servidor {
    public static void main(String[] args) {
        int puerto = 10000;
        byte[] buffer = new byte[1024];

        try {
            System.out.println("iniciado el servidor UDP");
            DatagramSocket socketUDP = new DatagramSocket(puerto);
            while (true) {
                DatagramPacket peticion = new DatagramPacket(buffer, buffer.length);

                socketUDP.receive(peticion);
                System.out.println("recibo la info del cliente");
                String mensaje = new String(peticion.getData());
                System.out.println(mensaje);

                int puertoClient = peticion.getPort();
                InetAddress direccion = peticion.getAddress();
// responder
                mensaje = "hola mundo desde el servidor";
                buffer = mensaje.getBytes();

                DatagramPacket respuesta = new DatagramPacket(buffer, buffer.length, direccion, puertoClient);
                System.out.println("envio informacion al cliente");
                socketUDP.send(respuesta);}


        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
