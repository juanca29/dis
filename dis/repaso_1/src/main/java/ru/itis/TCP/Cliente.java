package ru.itis.TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {


    public static void main(String[] args) {
        String host = "127.0.0.1";
        int port = 10000;
        Socket sc;
        // string de entrada desde el cliente
        DataInputStream in;
        // string de respuesta para el servidor
        DataOutputStream out;
        try {
            sc = new Socket(host, port);
            in = new DataInputStream(sc.getInputStream());
            out = new DataOutputStream(sc.getOutputStream());

            System.out.println("escriba un mensaje para el servidor");

            Scanner consola = new Scanner(System.in);
            String s = consola.nextLine();

            out.writeUTF("hola mundo desde el cliente, esto fue ingresado en la consola " + s);

            String mensaje = in.readUTF();
            System.out.println(mensaje);

            sc.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
