package ru.itis.TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
    public static void main(String[] args) {
        int puerto = 10000;
        // socket client
        Socket sc = null;
        // string de entrada desde el cliente
        DataInputStream in;
        // string de respuesta para el servidor
        DataOutputStream out;
        try {
            ServerSocket servidor = new ServerSocket(puerto);
            System.out.println("servidor iniciado");
            while (true) {
                sc = servidor.accept();
                System.out.println("el cliente conectado");
                in = new DataInputStream(sc.getInputStream());
                out = new DataOutputStream(sc.getOutputStream());

                String mensaje = in.readUTF();
                System.out.println(mensaje);
                out.writeUTF("hola mundo desde el servidor");
                sc.close();
                System.out.println("el cliente se desconecto");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
