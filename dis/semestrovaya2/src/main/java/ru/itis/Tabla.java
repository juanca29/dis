package ru.itis;

import ru.itis.db.BlockModel;
import ru.itis.db.DataModel;
import ru.itis.service.BlockRestServices;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.SQLException;

import static ru.itis.service.BlockRestServices.*;
import static ru.itis.service.DbWorker.EstablishConnection;

public class Tabla extends JFrame {

    private JTable tabla = null;
    DefaultTableModel modelo = null;
    JScrollPane desplazamiento = null;
    JButton botonenprimir = new JButton("imprimr");

    JLabel label = new JLabel("Add data");
    JLabel label1 = new JLabel("add name");
    JTextField data = new JTextField();
    JTextField name = new JTextField();
    JButton send = new JButton("Submit");


    public Tabla() throws SQLException {
        String[] columnas = {"prevhash", "data", "name", "signature","ts", "publickey"};
        tabla = new JTable();
        //tabla.setBounds(2,2,500,400);
        modelo = new DefaultTableModel();
        desplazamiento = new JScrollPane(tabla);

        // Parametros de la ventana
        this.setTitle("JTable");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        // Modelo de la tabla
        modelo.setColumnIdentifiers(columnas);

        // Barras de desplazamiento
        desplazamiento.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        desplazamiento.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        // Propiedades de la tabla
        tabla.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        tabla.setFillsViewportHeight(true);

        tabla.setModel(modelo);

        // Agregamos datos
        this.agregarDatos(modelo);

        send.setBounds(470, 40, 100, 30);
        send.addActionListener(e -> {
            try {
                EstablishConnection();
                String datas = data.getText();
                String names = name.getText();
                //System.out.println(datas + " " + names);
                DataModel model = new DataModel(datas, names);
                System.out.println(model.getData() + "--" + model.getName());
                BlockRestServices service = new BlockRestServices();
                service.getChain();
                updateDB();
                createBlock(new DataModel(model.getData(), model.getName()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        desplazamiento.setBounds(2,80,550,480);
        data.setBounds(10, 40, 200, 30);
        name.setBounds(250, 40, 200, 30);
        label.setBounds(10, 10, 200, 30);
        label1.setBounds(250, 10, 300, 30);


        // Agregando elementos a la ventana
        this.getContentPane().add(desplazamiento);
        this.getContentPane().add(label);
        this.getContentPane().add(label1);
        this.getContentPane().add(data);
        this.getContentPane().add(name);
        this.getContentPane().add(send);
        this.pack();


    }

    private void agregarDatos(DefaultTableModel modelo) throws SQLException {
        // Borramos todas las filas en la tabla
        modelo.setRowCount(0);

        // Creamos los datos de una fila de la tabla
        Object[] datosFila = {"prevhash", "data", "name", "signature","ts", "publickey"};

        // agregamos esos datos
        //modelo.addRow(datosFila);

        // Agregamos MUCHOS mas datos
        for(BlockModel x: getChain()) {
            datosFila[0] = x.getPrevhash();
            datosFila[2] = x.getData().getName();
            datosFila[1] = x.getData().getData();
            datosFila[3] = x.getSignature();
            datosFila[4] = x.getTs();
            datosFila[5] = x.getPublickey();

            modelo.addRow(datosFila);
        }
    }

    static public void main(String[] args) throws SQLException {
        //Tabla tabla = new Tabla();
        JFrame tabla = new Tabla();
        tabla.setSize(600,600);
        tabla.setLocationRelativeTo(null);
        tabla.setVisible(true);

    }
}
