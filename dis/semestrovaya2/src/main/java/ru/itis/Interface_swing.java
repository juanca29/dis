package ru.itis;

import lombok.SneakyThrows;
import ru.itis.db.DataModel;
import ru.itis.service.*;


import javax.swing.*;
import javax.xml.crypto.Data;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import static java.awt.Color.blue;
import static ru.itis.service.BlockRestServices.*;

import ru.itis.db.BlockChain;


import static ru.itis.service.DbWorker.EstablishConnection;

public class Interface_swing extends JFrame {

    public Interface_swing() throws Exception {
        //create jframe
        JFrame f = new JFrame("Blockchain");
        f.setSize(600, 400);
        f.setLocation(100, 150);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setDefaultLookAndFeelDecorated(true);
        //create labels
        //JLabel block = new JLabel(String.valueOf(getChain().get(getChain().size() - 1)));
        JLabel label = new JLabel("Add data");
        JLabel label1 = new JLabel("add name");
        label.setBounds(10, 10, 200, 30);
        label1.setBounds(250, 10, 300, 30);
        //block.setBounds(50, 200, 300, 30);
        //create textlines and button
        JTextField data = new JTextField();
        JTextField name = new JTextField();
        data.setBounds(10, 40, 200, 30);
        name.setBounds(250, 40, 200, 30);
        JButton send = new JButton("Submit");
        send.setBounds(470, 40, 100, 30);

        send.addActionListener(e -> {
            try {
                EstablishConnection();
                String datas = data.getText();
                String names = name.getText();
                //System.out.println(datas + " " + names);
                DataModel model = new DataModel(datas, names);
                System.out.println(model.getData() + "--" + model.getName());
                BlockRestServices service = new BlockRestServices();
                service.getChain();
                updateDB();
                createBlock(new DataModel(model.getData(), model.getName()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        // add info
        f.add(label);
        f.add(data);
        f.add(label1);
        f.add(name);
        //f.add(block);
        f.add(send);

        f.setLayout(null);
        f.setVisible(true);


    }
}
