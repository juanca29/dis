package ru.itis;

import java.sql.*;

public class Homework5join {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/Homework5";
        try (Connection conn = DriverManager.getConnection(url, "postgres", "Jcss2002")) {
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("select id, name, age, id_car, model from drivers join cars on  drivers.id  = cars.id_driver");

            System.out.println("------------------------------------------------------------------");
            System.out.println("id | name | age | id_car | model |");
            System.out.println("------------------------------------------------------------------");

            while (rs.next()) {
                System.out.println(rs.getString("id") + "|"
                        + rs.getString("name") + "|" + rs.getString("age") + "|" + rs.getString("id_car") + "|" + rs.getString("model"));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
