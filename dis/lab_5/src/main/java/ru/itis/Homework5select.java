package ru.itis;

import java.sql.*;

public class Homework5select {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/Homework5";
        try (Connection conn = DriverManager.getConnection(url, "postgres", "Jcss2002")) {
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("SELECT id_car, model, id_driver FROM cars");

            System.out.println("------------------------------------------------------------------");
            System.out.println("id_car | model | id_driver");
            System.out.println("------------------------------------------------------------------");

            while (rs.next()) {
                System.out.println(rs.getString("id_car") + "|"
                        + rs.getString("model") + "|" + rs.getString("id_driver"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
