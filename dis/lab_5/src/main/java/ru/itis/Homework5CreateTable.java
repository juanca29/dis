package ru.itis;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class Homework5CreateTable {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/Homework5";
        try (Connection conn = DriverManager.getConnection(url,"postgres","Jcss2002")){
            Statement statement = conn.createStatement();
            statement.execute("CREATE TABLE drivers (id bigint NOT NULL primary key, name varchar(255), age bigint)");
            statement.executeUpdate("insert into drivers (id, name, age) values (1,'danil', 28)," +
                    "(2,'kamil', 29), (3,'jhon', 25),(4,'robert', 35),(5,'pedro', 62),(6,'nikita', 20)");
            statement.execute("CREATE TABLE cars(id_car bigint NOT NULL, model varchar(255), id_driver bigint NOT NULL primary key)");
            statement.executeUpdate("insert into cars (id_car, model, id_driver) values (6,'BMW', 4)," +
                    "(3,'Audi', 2), (1,'lada', 5),(2,'hyundai', 1),(5,'jeep', 6),(4,'Audi', 3)");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

