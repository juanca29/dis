package ru.itis.laboratory1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class laboratory1 {
    public static void main(String[] args)  {

        try {
            Applab1 a = new Applab1("C:\\Users\\ACER\\Desktop\\EURRUB.csv","C:\\Users\\ACER\\Desktop\\USDRUB.csv");
        } catch (FileNotFoundException e) {
            System.out.println("502 — не найден файл");
        }


    }
    public static class Applab1 {

        ArrayList<Ticket> listtikers1 = new ArrayList<>();
        ArrayList<Ticket> listtikers2 = new ArrayList<>();
        Applab1(String first_FileName , String second_FileName) throws FileNotFoundException {
            try {
                readFirst(first_FileName);
                readSecond(second_FileName);
                operations(listtikers1, listtikers2);
            }catch(Exception e){
                System.out.println("501 — неверное количество аргументов");
            }
        }
        private void operations(ArrayList<Ticket> listtikers1, ArrayList<Ticket> listtikers2){
            double medianfirst = median(listtikers1);
            double mediansecond = median(listtikers2);
            System.out.println("median for first file " + medianfirst);
            System.out.println("median for second file " + mediansecond);
            double deviationfirts = deviations(listtikers1, medianfirst);
            double deviationsecond = deviations(listtikers2,mediansecond);
            System.out.println("this is the first deviation " + deviationfirts);
            System.out.println("this is the second deviation " + deviationsecond );
            double covariance = covariances(listtikers1,listtikers2,medianfirst,mediansecond , deviationfirts ,deviationsecond);
            System.out.println("{ r:" + covariance + "}");




        }

        private double covariances(ArrayList<Ticket> list1, ArrayList<Ticket> list2, double medianfirst, double mediansecond, double deviationfirts , double deviationsecond) {
            double sum_multiple_lists = 0;
            for (int i = 0; i < list1.size() ; i++) {

                sum_multiple_lists += list1.get(i).getClose() * list2.get(i).getClose() ;

            }
            return ((sum_multiple_lists/list1.size()) - (medianfirst*mediansecond))/(deviationfirts*deviationsecond);
        }

        private double deviations(ArrayList<Ticket> list, double median) {
            double sum_power = 0;
            for (Ticket x:list) {
                sum_power += Math.pow(x.getClose(),2);
            }
            double result = Math.sqrt ((sum_power/list.size())- Math.pow(median,2));
            return result;

        }

        private double median(ArrayList<Ticket> list) {
            double suma=0;
            for (Ticket x:list) {
                suma+= x.getClose();
            }
            return suma/list.size();
        }


        private void readSecond(String second_fileName) throws FileNotFoundException {
            int count =-1;
            FileInputStream fis = new FileInputStream(second_fileName);
            Scanner sc = new Scanner(fis);
            while (sc.hasNextLine()){
                if (count == -1) {
                    sc.nextLine();
                    count++;
                    continue;
                }
                String line = sc.nextLine();
                //System.out.println(line);
                Ticket ticket =loadticket(line);
                //System.out.println(ticket);
                listtikers2.add(ticket);
            }
            //System.out.println(listtikers2);
            Comparator<Ticket> comparator = Comparator.comparing(c -> Long.valueOf(c.getDate()));
            Collections.sort(listtikers2, comparator);
            System.out.println("this is the second file " + listtikers2);
        }

        private void readFirst(String first_fileName) throws FileNotFoundException {
            int count =-1;
            FileInputStream fis = new FileInputStream(first_fileName);
            Scanner sc = new Scanner(fis);
            while (sc.hasNextLine()){
                if (count == -1) {
                    sc.nextLine();
                    count++;
                    continue;
                }
                String line = sc.nextLine();
                //System.out.println(line);
                Ticket ticket =loadticket(line);
                //System.out.println(ticket);
                listtikers1.add(ticket);
            }
            //System.out.println(listtikers1);
            Comparator<Ticket> comparator = Comparator.comparing(c -> Long.valueOf(c.getDate()));
            Collections.sort(listtikers1, comparator);
            System.out.println("this is the firts file " + listtikers1);


        }

        private Ticket loadticket(String line) {
            String[] values = line.split(";");
            String tiker = values[0];
            String d = values[2];
            long date = Long.parseLong(d);
            String c = values[4];
            double close = Double.parseDouble(c);
            return new Ticket(tiker,date,close);
        }
    }
    public static class Ticket{
        private String ticker;
        private long date;
        private double close;

        public Ticket(String ticker, long date, double close) {
            this.ticker = ticker;
            this.date = date;
            this.close = close;
        }

        public String getTicker() {
            return ticker;
        }

        public void setTicker(String ticker) {
            this.ticker = ticker;
        }

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }

        public double getClose() {
            return close;
        }

        public void setClose(double close) {
            this.close = close;
        }
        public String toLine(){
            return ticker + "," + date + "," + close;
        }

        @Override
        public String toString() {
            return "Ticket{" +
                    "ticker='" + ticker + '\'' +
                    ", date=" + date +
                    ", close=" + close +
                    '}';
        }
    }
}

