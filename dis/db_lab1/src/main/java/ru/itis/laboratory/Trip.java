package ru.itis.laboratory;

public class Trip {
    private Client first;
    private Driver second;
    private String date;

    public Trip(Client first, Driver second, String date) {
        this.first = first;
        this.second = second;
        this.date = date;
    }

    public Client getFirst() {
        return first;
    }

    public void setFirst(Client first) {
        this.first = first;
    }

    public Driver getSecond() {
        return second;
    }

    public void setSecond(Driver second) {
        this.second = second;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "first=" + first +
                ", second=" + second +
                ", date='" + date + '\'' +
                '}';
    }
}
