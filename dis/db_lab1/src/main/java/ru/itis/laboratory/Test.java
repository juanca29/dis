package ru.itis.laboratory;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Test {
    public static void main(String[] args) {
        Driver a = new Driver("carlos", "+790890304", 1, new Car("kia", "hbfd6d566d"));
        Client b = new Client("pedro", "+6464564564", 2);
        Trip c = new Trip(b, a, "13/09/2021");


        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File("client.json"), c);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(c.toString());
    }
}
