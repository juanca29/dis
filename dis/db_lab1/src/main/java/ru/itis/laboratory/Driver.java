package ru.itis.laboratory;

public class Driver {
    private String name;
    private String phone;
    private int id;
    private Car car;

    public Driver(String name, String phone, int id, Car car) {
        this.name = name;
        this.phone = phone;
        this.id = id;
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", id=" + id +
                ", car=" + car +
                '}';
    }
}


